# Airport Distance Calculator

Airport Distance Calculator is a .NET Core project that calculates distance between two airports, based on their IATA codes.

## Installation

### Cloning
First of all, clone the repository with one of the following commands (for HTTP and SSH):

```cmd
git clone https://bitbucket.org/peter1820/airport-distance-calculator.git
```

```cmd
git clone git@bitbucket.org:peter1820/airport-distance-calculator.git
```

### Building
Then the project can be built with any .NET Core compiler available. NuGet packages usually are automatically restored, but in some exceptional cases, user intervention may be needed.


For Windows the build command is:
```cmd
dotnet build
```

Also, can be imported into Visual Studio and built directly from there.

### Running

After the build is done the project can be launched by executing the following command:

```cmd
dotnet run
```

Be careful, as this command needs to be executed from the AirportDistanceCalculator folder, otherwise you will get the message "Couldn't find a project to run".

From Visual Studio it can be run with the help of IIS Express, or as a separate program also.

The project now will be running and it can be accessed at two ports:

```url
https://localhost:5001/
http://localhost:5000
```

## Usage

While accessing the application exposed endpoints, Swagger will automatically load up exposing the actions that can be done on the exposed REST API.

The only possible action is to calculate the distance between two airports, based on IATA codes. For this a request needs to be made, as in the following example:

```
https://localhost:5001/api/distance/{IATA},{IATA}
```