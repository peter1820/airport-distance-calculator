﻿namespace AirportDistanceCalculator.Services
{
    using Models;

    /// <summary>
    /// Service used to handle distance-related actions.
    /// </summary>
    public interface IDistanceService
    {
        /// <summary>
        /// Calculate the distance between two points on the planet.
        /// </summary>
        /// <param name="location1">The first location.</param>
        /// <param name="location2">The second location.</param>
        /// <returns>The distance between the two locations.</returns>
        double CalculateDistance(Location location1, Location location2);
    }
}