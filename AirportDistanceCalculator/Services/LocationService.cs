﻿namespace AirportDistanceCalculator.Services
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Configuration;
    using Exceptions;
    using Models;

    /// <summary>
    /// Service used to handle location-related actions.
    /// </summary>
    public class LocationService : ILocationService
    {
        /// <summary>
        /// THe HTTP client factory instance.
        /// </summary>
        private readonly IHttpClientFactory clientFactory;

        /// <summary>
        /// The application configuration instance.
        /// </summary>
        private readonly IAppConfiguration appConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationService" /> class.
        /// </summary>
        /// <param name="clientFactory">The HTTP client factory instance.</param>
        /// <param name="appConfiguration">The application configuration instance.</param>
        public LocationService(IHttpClientFactory clientFactory, IAppConfiguration appConfiguration)
        {
            this.clientFactory = clientFactory;
            this.appConfiguration = appConfiguration;
        }

        /// <summary>
        /// Retrieves location coordinates based on airport IATA codes.
        /// </summary>
        /// <param name="iata">The airport IATA code.</param>
        /// <returns>The airport coordinates.</returns>
        public async Task<Location> GetLocationByIata(string iata)
        {
            Airport result;

            try
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, $"{appConfiguration.AirportsApiAddress}/{iata}"))
                {
                    var client = clientFactory.CreateClient();
                    var response = await client.SendAsync(request);

                    result = await response.Content.ReadAsAsync<Airport>();
                }
            }
            catch (Exception)
            {
                throw new LocationRetrievalException();
            }

            return result.Location;
        }
    }
}
