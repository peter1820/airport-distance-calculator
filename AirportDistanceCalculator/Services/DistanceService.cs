﻿namespace AirportDistanceCalculator.Services
{
    using System;
    using Configuration;
    using ExtensionMethods;
    using Models;

    /// <summary>
    /// Service used to handle distance-related actions.
    /// </summary>
    public class DistanceService : IDistanceService
    {
        /// <summary>
        /// The earth's radius represented in miles.
        /// </summary>
        private const int MilesRadius = 3960;

        /// <summary>
        /// The earth's radius represented in kilometers.
        /// </summary>
        private const int KilometerRadius = 6371;

        /// <summary>
        /// The application configuration instance.
        /// </summary>
        private readonly IAppConfiguration appConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistanceService" /> class.
        /// </summary>
        /// <param name="appConfiguration">The application configuration instance.</param>
        public DistanceService(IAppConfiguration appConfiguration)
        {
            this.appConfiguration = appConfiguration;
        }

        /// <summary>
        /// Calculate the distance between two points on the planet.
        /// </summary>
        /// <param name="location1">The first location.</param>
        /// <param name="location2">The second location.</param>
        /// <returns>The distance between the two locations, based on the configurable measurement unit.</returns>
        public double CalculateDistance(Location location1, Location location2)
        {
            var c = Calculate(location1, location2);

            var earthRadius = 0;

            switch (appConfiguration.MeasuringUnit)
            {
                case MeasuringUnit.Kilometers:
                    earthRadius = KilometerRadius;
                    break;
                case MeasuringUnit.Miles:
                    earthRadius = MilesRadius;
                    break;
            }

            return earthRadius * c;
        }

        /// <summary>
        /// Calculate the distance between two coordinate sets.
        /// </summary>
        /// <param name="location1">The first coordinate set.</param>
        /// <param name="location2">The second coordinate set.</param>
        /// <returns>The distance between the two points.</returns>
        private static double Calculate(Location location1, Location location2)
        {
            var latitudeDifference = (location2.Latitude - location1.Latitude).ToRadians();
            var longitudeDifference = (location2.Longitude - location1.Longitude).ToRadians();

            var a = Math.Sin(latitudeDifference / 2) * Math.Sin(latitudeDifference / 2) +
                    Math.Cos(location1.Latitude.ToRadians()) * Math.Cos(location2.Latitude.ToRadians()) *
                    Math.Sin(longitudeDifference / 2) * Math.Sin(longitudeDifference / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            return c;
        }
    }
}
