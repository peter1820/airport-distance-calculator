﻿namespace AirportDistanceCalculator.Services
{
    using System.Threading.Tasks;
    using Models;

    /// <summary>
    /// Service used to handle location-related actions.
    /// </summary>
    public interface ILocationService
    {
        /// <summary>
        /// Retrieves location coordinates based on airport IATA codes.
        /// </summary>
        /// <param name="iata">The airport IATA code.</param>
        /// <returns>The airport coordinates.</returns>
        Task<Location> GetLocationByIata(string iata);
    }
}