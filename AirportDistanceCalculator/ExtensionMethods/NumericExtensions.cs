﻿namespace AirportDistanceCalculator.ExtensionMethods
{
    using System;

    /// <summary>
    /// Class used to store extension methods for numeric types.
    /// </summary>
    public static class NumericExtensions
    {
        /// <summary>
        /// Converts a degree to radian representation.
        /// </summary>
        /// <param name="value">The degree to be converted.</param>
        /// <returns>The converted value, represented in radians.</returns>
        public static double ToRadians(this double value)
        {
            return (Math.PI / 180) * value;
        }
    }
}
