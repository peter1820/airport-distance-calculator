﻿namespace AirportDistanceCalculator.Exceptions
{
    using System;

    /// <summary>
    /// Exception class, thrown when something goes bad in retrieving airport locations. 
    /// </summary>
    public class LocationRetrievalException : Exception
    {
        /// <summary>
        /// The default error message.
        /// </summary>
        private const string DefaultMessage = "An error occurred while trying to obtain data regarding airport location. Please try again later.";

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationRetrievalException" /> class.
        /// </summary>
        public LocationRetrievalException()
            : base(DefaultMessage)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationRetrievalException" /> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        public LocationRetrievalException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocationRetrievalException" /> class.
        /// </summary>
        /// <param name="message">The exception message.</param>
        /// <param name="inner">The inner exception.</param>
        public LocationRetrievalException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
