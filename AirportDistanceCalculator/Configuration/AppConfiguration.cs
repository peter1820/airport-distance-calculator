﻿namespace AirportDistanceCalculator.Configuration
{
    using Models;

    /// <summary>
    /// Configuration class used throughout the application.
    /// </summary>
    public class AppConfiguration : IAppConfiguration
    {
        /// <summary>
        /// Gets or sets the address for the API used to retrieve airport information.
        /// </summary>
        public string AirportsApiAddress { get; set; }

        /// <summary>
        /// Gets or sets the measuring unit type used in the application.
        /// </summary>
        public MeasuringUnit MeasuringUnit { get; set; }
    }
}
