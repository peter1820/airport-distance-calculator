﻿namespace AirportDistanceCalculator.Configuration
{
    using Models;

    /// <summary>
    /// Configuration used throughout the application.
    /// </summary>
    public interface IAppConfiguration
    {
        /// <summary>
        /// Gets or sets the address for the API used to retrieve airport information.
        /// </summary>
        string AirportsApiAddress { get; set; }

        /// <summary>
        /// Gets or sets the measuring unit type used in the application.
        /// </summary>
        MeasuringUnit MeasuringUnit { get; set; }
    }
}