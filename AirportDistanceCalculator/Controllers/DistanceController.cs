﻿namespace AirportDistanceCalculator.Controllers
{
    using System.ComponentModel.DataAnnotations;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Services;

    /// <summary>
    /// Controller responsible for distance-based calculations.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DistanceController : ControllerBase
    {
        /// <summary>
        /// The distance service instance.
        /// </summary>
        private readonly IDistanceService distanceService;

        /// <summary>
        /// The location service instance.
        /// </summary>
        private readonly ILocationService locationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DistanceController" /> class.
        /// </summary>
        /// <param name="distanceService">The distance service instance.</param>
        /// <param name="locationService">The location service instance.</param>
        public DistanceController(IDistanceService distanceService, ILocationService locationService)
        {
            this.distanceService = distanceService;
            this.locationService = locationService;
        }

        /// <summary>
        /// Get the distance between two airports by IATA codes.
        /// </summary>
        /// <param name="iata1">The IATA code for the first airport.</param>
        /// <param name="iata2">The IATA code for the second airport.</param>
        /// <returns>The distance between the two airports.</returns>
        /// <response code="200">Returns the distance.</response>
        /// <response code="400">If any of the parameters are invalid.</response> 
        [HttpGet]
        [Route("{iata1},{iata2}")]
        [Produces("application/json")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<double>> GetDistance([Required, StringLength(3, MinimumLength = 3)]string iata1, [Required, StringLength(3, MinimumLength = 3)]string iata2)
        {
            double distance = 0;

            if (iata1 != iata2)
            {
                var location1 = await locationService.GetLocationByIata(iata1.ToUpper());
                var location2 = await locationService.GetLocationByIata(iata2.ToUpper());

                distance = distanceService.CalculateDistance(location1, location2);
            }

            return Ok(distance);
        }
    }
}
