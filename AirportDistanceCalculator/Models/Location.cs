﻿namespace AirportDistanceCalculator.Models
{
    using Newtonsoft.Json;

    /// <summary>
    /// Class used to represent a location by latitude and longitude coordinates.
    /// </summary>
    public class Location
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Location" /> class.
        /// </summary>
        public Location()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Location" /> class.
        /// </summary>
        /// <param name="latitude">The point's latitude.'</param>
        /// <param name="longitude">THe point's longitude.'</param>
        public Location(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        /// <summary>
        /// Gets or sets the location latitude coordinates.
        /// </summary>
        [JsonProperty("lat")]
        public double Latitude { get; set; }

        /// <summary>
        /// Gets or sets the location longitude coordinates.
        /// </summary>
        [JsonProperty("lon")]
        public double Longitude { get; set; }
    }
}
