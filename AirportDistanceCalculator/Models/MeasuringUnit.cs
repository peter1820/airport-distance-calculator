﻿namespace AirportDistanceCalculator.Models
{
    /// <summary>
    /// An enum containing possible measuring unit types.
    /// </summary>
    public enum MeasuringUnit
    {
        /// <summary>
        /// Imperial miles units.
        /// </summary>
        Miles,

        /// <summary>
        /// Kilometers units.
        /// </summary>
        Kilometers
    }
}
