﻿namespace AirportDistanceCalculator.Models
{
    using Newtonsoft.Json;

    /// <summary>
    /// Class representing an airport.
    /// </summary>
    public class Airport
    {
        /// <summary>
        /// Gets or sets the airport country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the airport's city IATA code.
        /// </summary>
        [JsonProperty("city_iata")]
        public string CityIata { get; set; }

        /// <summary>
        /// Gets or sets the airport IATA code.
        /// </summary>
        public string Iata { get; set; }

        /// <summary>
        /// Gets or sets the airport city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the time-zone region name.
        /// </summary>
        [JsonProperty("timezone_region_name")]
        public string TimezoneRegionName { get; set; }

        /// <summary>
        /// Gets or sets the country's IATA code.
        /// </summary>
        [JsonProperty("country_iata")]
        public string CountryIata { get; set; }

        /// <summary>
        /// Gets or sets the airport rating.
        /// </summary>
        public int Rating { get; set; }

        /// <summary>
        /// Gets or sets the airport name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the airport location.
        /// </summary>
        public Location Location { get; set; }

        /// <summary>
        /// Gets or sets the airport type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the number of hubs in the airport.
        /// </summary>
        public int Hubs { get; set; }
    }
}
